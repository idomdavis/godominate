# godominate

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/godominate/main?style=plastic)](https://bitbucket.org/idomdavis/godominate/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/godominate?style=plastic)](https://bitbucket.org/idomdavis/godominate/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/godominate?style=plastic)](https://bitbucket.org/idomdavis/godominate/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/godominate)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

A way of showing Mee6 discord stats when being *way* too competitive about the 
whole chat XP thing. Best run using a config file 
(see [example](example-config.json)) with:

```
godominate -c <config-file>
```

Build from source with `make`, `make pi` (for raspberry Pi) or `make linux`.
