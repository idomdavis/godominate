package config

import (
	"bitbucket.org/idomdavis/goconfigure"
)

// Colour settings for dissonance.
type Colour struct {
	// Leading rank colour.
	Leading int

	// Trailing rank colour.
	Trailing int

	// Name bar colour.
	Name int

	// Level colour.
	Level int

	// Label colour.
	Label int

	// Value colour.
	Value int
}

// Description for the colour config block.
func (c *Colour) Description() string {
	return "Colour settings"
}

// Register the colour options.
func (c *Colour) Register(opts goconfigure.OptionSet) {
	opts.Add(opts.Option(&c.Leading, 10, "leading-colour", "Leading rank colour"))
	opts.Add(opts.Option(&c.Trailing, 9, "trailing-colour", "Trailing rank colour"))
	opts.Add(opts.Option(&c.Name, 14, "name-colour", "Name bar colour"))
	opts.Add(opts.Option(&c.Level, 12, "level-colour", "Level colour"))
	opts.Add(opts.Option(&c.Label, 8, "label-colour", "Label colour"))
	opts.Add(opts.Option(&c.Value, 7, "value-colour", "Value colour"))
}

// Data set on the colour block.
func (c *Colour) Data() interface{} {
	return c
}
