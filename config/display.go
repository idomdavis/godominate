package config

import "bitbucket.org/idomdavis/goconfigure"

// Display settings for dissonance.
type Display struct {
	// Width of display.
	Width int

	// Show n users.
	Show int

	// Separator used between columns.
	Separator string
}

// Description for the display config block.
func (d *Display) Description() string {
	return "Display settings"
}

// Register the display options.
func (d *Display) Register(opts goconfigure.OptionSet) {
	opts.Add(opts.Option(&d.Width, 25, "width", "Width of display"))
	opts.Add(opts.Option(&d.Show, 4, "show", "Show n users"))
	opts.Add(opts.Option(&d.Separator, "", "separator", "Column separator"))
}

// Data set on the display block.
func (d *Display) Data() interface{} {
	return d
}
