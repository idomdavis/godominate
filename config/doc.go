// Package config provides goconfigure compatible configuration blocks to
// configure godominate from the command line, environment or config file.
package config
