package config_test

import (
	"fmt"

	"bitbucket.org/idomdavis/goconfigure"
	"bitbucket.org/idomdavis/godominate/config"
)

// Ordinarily this would be os.Args[1:], however for the purposes of this
// example we pull them from here.
var args = []string{"-guild", "1234", "-user", "2345"}

func Example() {
	s, err := config.Parse(args, goconfigure.ConsoleReporter{})

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(s.Mee6.Guild)

	// Output:
	// Mee6 settings: [Guild:1234, User:2345]
	// Display settings: [Separator:, Show:4, Width:25]
	// Colour settings: [Label:8, Leading:10, Level:12, Name:14, Trailing:9, Value:7]
	// 1234
}
