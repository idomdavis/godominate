package config

import "bitbucket.org/idomdavis/goconfigure"

// Mee6 settings for dissonance.
type Mee6 struct {
	// Guild ID.
	Guild string

	// User ID to watch as primary user.
	User string
}

// Description for the mee6 config block.
func (m *Mee6) Description() string {
	return "Mee6 settings"
}

// Register the mee6 options.
func (m *Mee6) Register(opts goconfigure.OptionSet) {
	opts.Add(opts.Option(&m.Guild, "", "guild",
		"Snowflake ID for the guild to watch"))
	opts.Add(opts.Option(&m.User, "", "user",
		"Snowflake ID for the primary user to watch"))
}

// Data set on the mee6 block.
func (m *Mee6) Data() interface{} {
	return m
}

// Valid returns true if the config is valid.
func (m *Mee6) Valid() bool {
	return m.Guild != "" && m.User != ""
}
