package config_test

import (
	"fmt"

	"bitbucket.org/idomdavis/godominate/config"
)

func ExampleMee6_Valid() {
	c := config.Mee6{}
	fmt.Println(c.Valid())

	// Output: false
}
