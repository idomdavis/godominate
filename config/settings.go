package config

import (
	"fmt"

	"bitbucket.org/idomdavis/goconfigure"
)

// Settings for the loader.
type Settings struct {
	Mee6    Mee6
	Display Display
	Colour  Colour

	Underlying *goconfigure.Settings
}

// Parse the config options into a settings struct.
func Parse(args []string, reporter goconfigure.Reporter) (Settings, error) {
	settings := Settings{}
	conf := goconfigure.NewSettings("GODOMINATE")

	conf.AddHelp()
	conf.AddConfigFile()
	conf.Add(&settings.Mee6)
	conf.Add(&settings.Display)
	conf.Add(&settings.Colour)

	if err := conf.ParseUsing(args, reporter); err != nil {
		return settings, fmt.Errorf("failed to configure godominate: %w", err)
	}

	settings.Underlying = conf

	return settings, nil
}
