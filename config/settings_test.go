package config_test

import (
	"testing"

	"bitbucket.org/idomdavis/goconfigure"
	"bitbucket.org/idomdavis/godominate/config"
)

func TestParse(t *testing.T) {
	t.Run("Failure to parse the config errors", func(t *testing.T) {
		_, err := config.Parse([]string{"--die"}, goconfigure.NullReporter{})

		if err == nil {
			t.Errorf("Expected error with invalid config")
		}
	})
}
