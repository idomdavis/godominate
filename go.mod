module bitbucket.org/idomdavis/godominate

go 1.16

require (
	bitbucket.org/idomdavis/goconfigure v0.5.9
	bitbucket.org/idomdavis/gohttp v0.4.3
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
)
