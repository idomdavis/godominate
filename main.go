package main

import (
	"os"

	"bitbucket.org/idomdavis/goconfigure"
	"bitbucket.org/idomdavis/godominate/config"
	"bitbucket.org/idomdavis/godominate/monitor"
	"bitbucket.org/idomdavis/godominate/term"
	"github.com/sirupsen/logrus"
)

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})

	settings, err := config.Parse(os.Args[1:], goconfigure.LogrusReporter{
		Logger: logrus.StandardLogger(),
	})

	if err != nil {
		logrus.WithField("error", err).Fatal("Error configuring godominate")
		settings.Underlying.Usage(os.Stderr)
		os.Exit(1)
	}

	if !settings.Mee6.Valid() {
		settings.Underlying.Usage(os.Stderr)
		os.Exit(1)
	}

	if settings.Underlying.Help {
		settings.Underlying.Usage(os.Stdout)
		os.Exit(0)
	}

	defer term.Show()

	if err = monitor.Run(settings); err != nil {
		logrus.WithError(err).Error("Failed to load data")
	}
}
