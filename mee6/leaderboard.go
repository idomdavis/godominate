package mee6

// Leaderboard details.
type Leaderboard struct {
	Step     int
	Users    map[string]User
	Rankings []string
}

// User holds the relevant data for each user in the competition.
type User struct {
	Name       string
	Level      int
	Rank       int
	Messages   int
	Experience Experience
}

// Experience holds the experience data for each user.
type Experience struct {
	Total    int
	Current  int
	Required int
}

// NewExperience returns a new Experience block from a DetailedXp block in the
// Payload. If the Payload is malformed the Experience block will only be
// partially populated.
//nolint:gomnd
func NewExperience(xp []int) Experience {
	experience := Experience{}

	switch len(xp) {
	case 3:
		experience.Total = xp[2]
		fallthrough
	case 2:
		experience.Required = xp[1]
		fallthrough
	case 1:
		experience.Current = xp[0]
	}

	return experience
}

// NewLeaderboard returns a new Leaderboard from a Mee6 Payload.
func NewLeaderboard(payload Payload) Leaderboard {
	leaderboard := Leaderboard{
		Users:    make(map[string]User, len(payload.Players)),
		Rankings: make([]string, len(payload.Players)),
	}

	for _, xp := range payload.XpPerMessage {
		leaderboard.Step += xp
	}

	if len(payload.XpPerMessage) > 0 {
		leaderboard.Step /= len(payload.XpPerMessage)
	}

	for i, p := range payload.Players {
		leaderboard.Rankings[i] = p.ID
		leaderboard.Users[p.ID] = User{
			Name:       p.Username,
			Level:      p.Level,
			Rank:       i + 1,
			Messages:   p.MessageCount,
			Experience: NewExperience(p.DetailedXp),
		}
	}

	return leaderboard
}

// Rank returns the user at the giver rank.
func (l Leaderboard) Rank(rank int) User {
	rank--

	if rank < 0 {
		return User{}
	}

	if rank >= len(l.Rankings) {
		return User{}
	}

	return l.Users[l.Rankings[rank]]
}

// Top n users. The given user will always be listed, coming last if they are
// not in the top n, with the last in the top n being dropped.
func (l Leaderboard) Top(n int, user string) []string {
	users := make([]string, n)

	n--

	if n > len(l.Rankings) {
		n = len(l.Rankings)
	}

	for i := 0; i <= n; i++ {
		users[i] = l.Rankings[i]

		if l.Rankings[i] == user {
			user = ""
		}
	}

	if user != "" {
		users[len(users)-1] = user
	}

	return users
}
