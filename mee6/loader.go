package mee6

import (
	"fmt"

	"bitbucket.org/idomdavis/gohttp/conversation"
)

const url = "https://mee6.xyz/api/plugins/levels/leaderboard"

// Load the Payload from the mee6 API.
func Load(guild string) (Payload, error) {
	var leaderboard Payload

	request := conversation.Request{}
	response, err := request.Get(fmt.Sprintf("%s/%s", url, guild))

	if err != nil {
		return leaderboard, fmt.Errorf("failed to request leaderboard: %w", err)
	}

	defer func() { _ = response.Body.Close() }()

	if err = conversation.Unmarshal(response.Body, &leaderboard); err != nil {
		return leaderboard, fmt.Errorf("failed to parse leaderboard: %w", err)
	}

	return leaderboard, nil
}
