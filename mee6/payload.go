package mee6

// Payload data from mee6.
type Payload struct {
	Guild struct {
		Icon              string `json:"icon"`
		ID                string `json:"id"`
		LeaderboardURL    string `json:"leaderboard_url"`
		Name              string `json:"name"`
		AllowJoin         bool   `json:"allow_join"`
		InviteLeaderboard bool   `json:"invite_leaderboard"`
		Premium           bool   `json:"premium"`
	} `json:"guild"`
	Players []struct {
		Avatar        string `json:"avatar"`
		DetailedXp    []int  `json:"detailed_xp"`
		Discriminator string `json:"discriminator"`
		GuildID       string `json:"guild_id"`
		ID            string `json:"id"`
		Level         int    `json:"level"`
		MessageCount  int    `json:"message_count"`
		Username      string `json:"username"`
		XP            int    `json:"xp"`
	} `json:"players"`
	RoleRewards  []interface{} `json:"role_rewards"`
	XpPerMessage []int         `json:"xp_per_message"`

	BannerURL         interface{} `json:"banner_url"`
	Player            interface{} `json:"player"`
	UserGuildSettings interface{} `json:"user_guild_settings"`

	Page     int     `json:"page"`
	XpRate   float64 `json:"xp_rate"`
	Admin    bool    `json:"admin"`
	IsMember bool    `json:"is_member"`
}
