package monitor

import (
	"fmt"

	"bitbucket.org/idomdavis/godominate/config"
	"bitbucket.org/idomdavis/godominate/mee6"
	"bitbucket.org/idomdavis/godominate/term"
)

// Display the leaderboard data.
func Display(settings config.Settings, leaderboard mee6.Leaderboard) {
	term.Clear()
	term.Hide()

	for _, user := range leaderboard.Top(settings.Display.Show, settings.Mee6.User) {
		DisplayUser(settings, NewStats(user, leaderboard))
	}
}

// DisplayUser shows details for a user based on the stats and config settings.
//nolint:dupl,funlen
func DisplayUser(settings config.Settings, stats Stats) {
	s := settings.Display.Separator
	width := settings.Display.Width
	colour := settings.Colour
	levelColour := colour.Trailing
	msgs := " Msgs"

	if !stats.ShowBehind {
		levelColour = settings.Colour.Leading
	}

	if settings.Display.Separator == "" {
		msgs = "Msgs:"
	}

	write(fmt.Sprintf("%.2d", stats.User.Rank), levelColour)
	fmt.Print(term.Transition(s, levelColour, colour.Name))
	write(fmt.Sprintf(nameFormat(width, s), stats.User.Name), colour.Name)
	fmt.Print(term.Transition(s, colour.Name, colour.Level))
	write(fmt.Sprintf(" %.2d ", stats.User.Level), colour.Level)
	fmt.Println()

	if stats.ShowBehind {
		write("↓↓", colour.Label)
		fmt.Print(term.Transition(s, colour.Label, colour.Value))
		write(fmt.Sprintf(leftFormat(width, s, ""), stats.BehindXP), colour.Value)
		fmt.Print(term.Transition(s, colour.Value, colour.Label))
		write(msgs, colour.Label)
		fmt.Print(term.Transition(s, colour.Label, colour.Value))
		write(fmt.Sprintf(rightFormat(width, s, "≈"), stats.BehindMsgs), colour.Value)
		fmt.Println()
	}

	write("XP", colour.Label)
	fmt.Print(term.Transition(s, colour.Label, colour.Value))
	write(fmt.Sprintf(leftFormat(width, s, ""), stats.User.Experience.Total), colour.Value)
	fmt.Print(term.Transition(s, colour.Value, colour.Label))
	write(msgs, colour.Label)
	fmt.Print(term.Transition(s, colour.Label, colour.Value))
	write(fmt.Sprintf(rightFormat(width, s, ""), stats.User.Messages), colour.Value)
	fmt.Println()

	write("+1", colour.Label)
	fmt.Print(term.Transition(s, colour.Label, colour.Value))
	write(fmt.Sprintf(leftFormat(width, s, "XP"), stats.RequiredXP), colour.Value)
	fmt.Print(term.Transition(s, colour.Value, colour.Label))
	write(msgs, colour.Label)
	fmt.Print(term.Transition(s, colour.Label, colour.Value))
	write(fmt.Sprintf(rightFormat(width, s, "≈"), stats.RequiredMsgs), colour.Value)
	fmt.Println()

	if stats.ShowAhead {
		write("↑↑", colour.Label)
		fmt.Print(term.Transition(s, colour.Label, colour.Value))
		write(fmt.Sprintf(leftFormat(width, s, ""), stats.AheadXP), colour.Value)
		fmt.Print(term.Transition(s, colour.Value, colour.Label))
		write(msgs, colour.Label)
		fmt.Print(term.Transition(s, colour.Label, colour.Value))
		write(fmt.Sprintf(rightFormat(width, s, "≈"), stats.AheadMsgs), colour.Value)
		fmt.Println()
	}
}

const (
	columns    = 2
	pad        = 2
	separators = 2
)

func nameFormat(width int, separator string) string {
	const committed = 6

	width = width - committed - (separators * len([]rune(separator))) - pad

	return fmt.Sprintf(" %%-%ds ", width)
}

func leftFormat(width int, separator, units string) string {
	const label = 2

	if width%2 != 0 {
		width--
	}

	width = (width / columns) - label - pad
	width = width - (separators * len([]rune(separator))) - (len(units))

	return fmt.Sprintf(" %%-%dd%s ", width, units)
}

func rightFormat(width int, separator, prefix string) string {
	const label = 5

	if width%columns != 0 {
		width++
	}

	width = (width / columns) - label - pad
	width = width - len([]rune(separator)) - (len([]rune(prefix)))

	return fmt.Sprintf(" %s%%-%dd ", prefix, width)
}

func write(s string, colour int) {
	fmt.Print(term.Colour(s, colour, term.White))
}
