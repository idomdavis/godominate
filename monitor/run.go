package monitor

import (
	"time"

	"bitbucket.org/idomdavis/godominate/config"
	"bitbucket.org/idomdavis/godominate/mee6"
	"bitbucket.org/idomdavis/godominate/term"
	"github.com/sirupsen/logrus"
)

// Run the monitor, checking the results every minute.
func Run(settings config.Settings) error {
	for {
		if payload, err := mee6.Load(settings.Mee6.Guild); err != nil {
			term.Clear()
			logrus.WithError(err).Error("Failed to load stats")
		} else {
			Display(settings, mee6.NewLeaderboard(payload))
		}

		time.Sleep(time.Minute)
	}
}
