package monitor

import "bitbucket.org/idomdavis/godominate/mee6"

// Stats for a user.
type Stats struct {
	User mee6.User

	ShowAhead  bool
	ShowBehind bool

	RequiredXP   int
	RequiredMsgs int
	AheadXP      int
	AheadMsgs    int
	BehindXP     int
	BehindMsgs   int
}

// NewStats returns a new set of Stats for a Leaderboard.
func NewStats(id string, leaderboard mee6.Leaderboard) Stats {
	user := leaderboard.Users[id]
	previous := leaderboard.Rank(user.Rank + 1).Experience.Total
	next := leaderboard.Rank(user.Rank - 1).Experience.Total

	s := Stats{
		User:       user,
		ShowAhead:  user.Rank < len(leaderboard.Rankings),
		ShowBehind: user.Rank > 1,
		RequiredXP: user.Experience.Required - user.Experience.Current,
	}

	s.RequiredMsgs = s.RequiredXP / leaderboard.Step

	if previous > 0 {
		s.AheadXP = user.Experience.Total - previous
		s.AheadMsgs = s.AheadXP / leaderboard.Step
	}

	if next > 0 {
		s.BehindXP = next - user.Experience.Total
		s.BehindMsgs = s.BehindXP / leaderboard.Step
	}

	return s
}
