package term

import "fmt"

// Clear the screen and return to 1, 1 (top left).
func Clear() {
	fmt.Printf("\u001B[2J\u001B[1;1H")
}

// Hide the cursor.
func Hide() {
	fmt.Printf("\u001B[?25l")
}

// Show the cursor.
func Show() {
	fmt.Printf("\u001B[?25h")
}
