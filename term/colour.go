package term

import (
	"fmt"
)

// Base ANSI colours.
const (
	Black = iota
	Red
	Green
	Yellow
	Blue
	Magenta
	Cyan
	LightGrey
	DarkGrey
	BrightRed
	BrightGreen
	BrightYellow
	BrightBlue
	BrightMagenta
	BrightCyan
	White
)

// Colour will apply a foreground and background colour to a string.
func Colour(i interface{}, bg, fg int) string {
	return fmt.Sprintf("\u001B[48;5;%dm\u001B[38;5;%dm%v\u001B[0m", bg, fg, i)
}

// Transition from one background colour to the next.
func Transition(symbol string, from, to int) string {
	if symbol == "" {
		return symbol
	}

	return Colour(symbol, to, from)
}
