package term_test

import (
	"fmt"

	"bitbucket.org/idomdavis/godominate/term"
)

func ExampleColour() {
	fmt.Println(term.Colour(0, term.Black, term.DarkGrey))
	fmt.Println(term.Colour(1, term.Red, term.BrightRed))
	fmt.Println(term.Colour(2, term.Green, term.BrightGreen))
	fmt.Println(term.Colour(3, term.Yellow, term.BrightYellow))
	fmt.Println(term.Colour(4, term.Blue, term.BrightBlue))
	fmt.Println(term.Colour(5, term.Magenta, term.BrightMagenta))
	fmt.Println(term.Colour(6, term.Cyan, term.BrightCyan))
	fmt.Println(term.Colour(7, term.LightGrey, term.White))
	fmt.Println(term.Colour(8, term.Black, term.White),
		term.Colour(9, term.White, term.Black))

	// Output:
	// [48;5;0m[38;5;8m0[0m
	// [48;5;1m[38;5;9m1[0m
	// [48;5;2m[38;5;10m2[0m
	// [48;5;3m[38;5;11m3[0m
	// [48;5;4m[38;5;12m4[0m
	// [48;5;5m[38;5;13m5[0m
	// [48;5;6m[38;5;14m6[0m
	// [48;5;7m[38;5;15m7[0m
	// [48;5;0m[38;5;15m8[0m [48;5;15m[38;5;0m9[0m
}
